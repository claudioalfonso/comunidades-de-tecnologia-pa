Comunidades Paraenses de TI e Inovação
======================================

As comunidades paraenses de tecnologia e afins estão bombando na vida real e nos grupos no Telegram e WhatsApp! Participe você também. Esse repositório contém uma lista organizada das comunidades (e seus meios de comunicação) por temas na tecnologia.

Inspiração no [Repositório de Canais e Grupos Brasileiros de TI ](https://github.com/alexmoreno/telegram-br) e no [Repositório de Comunidades Potiguares de TI](https://gitlab.com/pbaesse/comunidades-ti-rn). Conheças várias outras comunidades lá também.

Para realizar pedidos de novas comunidades:
1. Crie um pull request para adicionar novas comunidades;
2. Retire o :triangular_flag_on_post: das comunidades com esse ícone e coloque nas novas comunidades adicionadas;
3. Avise no grupo do [ParaLivre no Telegram](http://t.me/paralivre) a alteração.
**OBS**: Caso não saiba usar o GitLab, basta pedir para que sua comunidade seja adicionada aqui no grupo do telegram do [ParaLivre](http://t.me/paralivre). 

# Eventos
- **Agenda TI Pará**: Agenda de Eventos de TI no Pará
  - [Site](https://www.agendatipara.com.br)
  - [Calendário](https://www.agendatipara.com.br/calendario)  
  - [Telegram](https://t.me/agendatipara)  
  - [Twitter](https://www.twitter.com/agendatipara)  
  - [Facebook](https://www.facebook.com/agendatipara)
  - [Instagram](https://www.instagram.com/agendatipara) 
  - [Linkedin](https://www.linkedin.com/company/agendatipara/)  

# Software Livre e Open Source
- **ParaLivre**: Comunidade Paraense de Software Livre
  - [Site](https://www.paralivre.org)
  - [Telegram](https://t.me/paralivre)  
  - [Twitter](http://www.twitter.com/paralivre_)
  - [Instagram](https://www.instagram.com/paralivre)
  
- **Centro de Competência em Software Livre - UFPA**
  - [Site](http://ccsl.ufpa.br)
 
# Linguagens de Programação
- **GruPy Belém**: Comunidade de Python de Belém
  - [Telegram](https://t.me/GruPy_Belem)
  - [WhatsApp](https://chat.whatsapp.com/ERHL4P2YfUqHIFlO9uorMr)
  
- **PHP Pará**: Elephants Pará
  - [Site](https://phppa.org/)
  - [Telegram](https://t.me/PHPPA)
  - [Twitter](https://twitter.com/phppara)
  - [Facebook](https://www.facebook.com/elephants.para)
  
- **Java Belém**: Galera do Java Belém PA
  - [WhatsApp](https://chat.whatsapp.com/KTfRmaqPRqJ4Oyn5jENfnn)
  - [Telegram](https://t.me/joinchat/LwNStBerdCoTZZIlrDjIPA)

- **Linguagem Egua**: Comunidade da Linguagem de Programação Egua
  - [Site](https://egua.tech/)
  - [GitHub](https://github.com/eguatech)
  - [Telegram Grupo](https://t.me/eguatech)
  - [Telegram Canal](https://t.me/canalegua)
 
# Programação e Desenvolvimento Aplicado e Ágil
- **Devs Norte**: Comunidade de desenvolvedores do Norte do Brasil
  - [Site](https://devsnorte.com/)
  - [GitHub](https://www.github.com/devsnorte/)
  - [Telegram](https://t.me/devsnorte)
  - [WhatsApp](https://chat.whatsapp.com/LiHQUNP4k7tBDLa5oXp2se)
  - [Facebook](https://www.facebook.com/devsnorte)
  - [Instagram](https://www.instagram.com/devsnorte)
  - [Twitter](https://twitter.com/devsnorte)
  - [Youtube](https://youtube.com/c/DevsNorte)
  - [Linkedin](https://www.linkedin.com/company/devsnorte/)
  - [Discord](https://discord.gg/V825KxKzcQ)
  
- **Tá Safo**: Tecnologias Abertas com Software Ágil, Fácil e Organizado
  - [Site](https://tasafo.org)
  - [Tá Safo Trampos](https://trampos.tasafo.org) - É o local para encontrar e anunciar vagas de empregos e currículos de membros da comunidade Tá Safo!
  - [Slack](tasafo.slack.com)
  - [Twitter](https://twitter.com/tasafo)
  - [Facebook](https://www.facebook.com/tasafo.comunidade)
  - [Instagram](https://www.instagram.com/comunidadetasafo)

- **Joomla Belém**: Grupo de Usuários Joomla! Belém 
  - [Listagem User Group](https://community.joomla.org/user-groups/south-america/brazil.html)
  - [Site](http://joomlabelem.paralivre.org)
  - [Telegram](https://t.me/joomlabelem)
  - [Instagram](https://www.instagram.com/joomlabelem)
  - [Twitter](https://twitter.com/joomlabelem)        
  - [Facebook](https://www.facebook.com/joomlabelem)  
  - [Linkedin](https://www.linkedin.com/company/joomlabelem/)  

- **Flutter Pará**: Grupo de Comunidade Flutter 
  - [WhatsApp](https://chat.whatsapp.com/LBJrF2x54gU4DxYYzMhgWt)
  - [Telegram](https://t.me/+R7RAxtBw-xQxNWRh)
   
# Desenvolvimento de Jogos
- **Bel Jogos**: Grupo de entusiastas, hobbistas, apreciadores e desenvolvedores de jogos eletrônicos em Belém
  - [Instagram](https://www.instagram.com/beljogos)
  - [Twitter](https://twitter.com/beljogos)
  - [Facebook](https://www.facebook.com/BeljogosPA)
  - [Youtube](https://www.youtube.com/channel/UCa0FvmBQz0JiActsGs5r5-A)  
  - [Flickr](https://www.flickr.com/photos/beljogos/) 
  - [Linkedin](https://www.linkedin.com/groups/1480417/)
  - [Discord](https://discord.gg/25DtKFsVMy)  
  
- **GameDevsPA**: Grupo multidisciplinar formado por diversos profissionais que trabalham em conjunto para o fortalecimento da indústria de jogos no estado do Pará.
  - [Instagram](https://www.instagram.com/gamedevspa)
  - [Facebook Fan Page](http://facebook.com/gamedevspa)
  - [Facebook Grupo](https://www.facebook.com/groups/117244738954945/)  
  - [Youtube](https://www.youtube.com/channel/UCOzB-hyIPS_DanMritkuHKA)  
  
# DEVOPS
- **DevOps Parauapebas**: Grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas
  - [Telegram](https://t.me/joinchat/A-G57xd_fjAnQwOzV_sdeQ)
  - [Instagram](https://instagram.com/devopspbs)
  - [GitLab](https://gitlab.com/devopspbs)
  - [MeetUp](https://meetup.com/devopspbs)    
  - [Site](https://devopspbs.org)    

# Maker
- **Arduino Parauapebas**: Grupo que promove eventos, projetos, grupo de estudo e todas as outras possibilidades envolvendo o Arduino e outros embarcados
  - [Youtube](https://www.youtube.com/channel/UCDSOUrnRptHkP9Atv-zO_qg)
  - [Facebook](http://www.facebook.com/ArduinoParauapebas)

# Banco de Dados, Data Platform
- **SQL Norte**: O grupo compartilha conhecimento sobre dados em eventos, promove o networking na Comunidade do Norte do País
  - [WhatsApp](https://chat.whatsapp.com/EzD5z7bpXc41fbW0TCw31Q)
  - [WhatsApp](https://chat.whatsapp.com/BCWb6O09jPA1VWZGgIygG7)  
  - [Telegram](https://t.me/joinchat/H3xPKxFX2iO8wKMyU55kiQ)
  - [Facebook](https://www.facebook.com/sqlnorte)
  - [Instagram](https://www.instagram.com/sqlnorte_oficial)
  - [Twitter](https://twitter.com/sqlnorte)
  - [Youtube](https://www.youtube.com/channel/UCBvWwvJQY0JaoHC2YgJrJ2Q)
  - [Linkedin](https://www.linkedin.com/company/sqlnorte)
  - [Meetup](https://www.meetup.com/pt-BR/SQL-Norte)
     
- **Power BI Carajás (SQL Carajás)**: Grupo de usuários do Power BI em Parauapebas e Região de Carajás
  - [MeetUp](https://www.meetup.com/PowerBI-Carajas)    

# Promoção e reforço da participação feminina na tecnologia
- **ArduLadies Belém**: Somos uma comunidade de mulheres makers apaixonadas por eletrônica, tecnologias, robótica e programação. 
  - [GitHub](https://github.com/arduladies-belem)
  - [Facebook](https://www.facebook.com/Arduladies-Bel%C3%A9m-259851468268070/)
  - [Instagram](https://www.instagram.com/arduladiesbelem/)
  - [Twitter](https://twitter.com/ArduladiesB)
  
- **DevOps Girls Parauapebas**
  - [WhatsApp](https://chat.whatsapp.com/L2L2emg0pCy5K1xoyP6IYE)
       
- **Django Girls Belém**
  - [Facebook](https://www.facebook.com/DjangoGirlsBelem)
  - [Instagram](https://www.instagram.com/djangogirlsbelem)
  - [Site](https://djangogirls.org/belem)
 
- **Manas Digitais**: Tem como objetivo a realização de práticas de caráter motivacional e informativo para promover a carreira na computação e áreas tecnológicas
  - [Facebook](https://www.facebook.com/manasdigitais)
  - [Instagram](https://www.instagram.com/manasdigitais)
  - [Site](http://meninas.sbc.org.br/index.php/portfolio/manas-digitais/)
 
- **Meninas da Geotecnologia**: Projeto de Extensão do IFPA campus Castanhal, chancelado pela Sociedade Brasileira de Computação.
  - [Facebook](https://www.facebook.com/meninasdageo)
  - [Instagram](https://www.instagram.com/meninasdageo)
  - [Telegram](https://t.me/meninasdageo)  
  - [Site](http://meninas.sbc.org.br/index.php/portfolio/meninas-da-geotecnologia/)
  - [PodCast](https://open.spotify.com/show/0lnKDEv9jX8tr9uyzB8NLv?si=BOsOIY3eRSGZhPfOcuw_6A&fbclid=IwAR1PenW3T-68DJXGb4NYoEydtHZUZU0jDT6JkY0fdcQSccF3J7xNeBD--5A)
  
- **Meninas Paid’éguas**: Tem como objetivo despertar o interesse e fomentar a inclusão de meninas estudantes do ensino médio em carreiras na área de Computação e Ciências Exatas, nos municípios de Belém e Castanhal.  
  - [Instagram](https://www.instagram.com/meninaspaideguas/)
  - [Facebook](https://www.facebook.com/Meninas-Pai-D%C3%A9guas-116728469720671/)
  - [Site](http://meninas.sbc.org.br/index.php/portfolio/meninas-paideguas/)
 
- **Paragobyte Girls**: O grupo tem como objetivo realizar ações que estimulem a participação e a formação de mulheres nas áreas de ciência e tecnologia
  - [Facebook](https://www.facebook.com/paragobytegirls)
  - [Instagram](https://www.instagram.com/paragobytegirls/)
  - [Site](http://meninas.sbc.org.br/index.php/portfolio/paragobytegirls/)
  
- **PyLadies Belém**: É um grupo internacional, com foco em ajudar mais mulheres a se tornarem participantes ativas e líderes da comunidade de código aberto Python 
  - [Facebook](https://www.facebook.com/pyladiesbelem)
  - [Instagram](https://www.instagram.com/pyladiesbelem)
    
- **Rails Girls Belém**: É uma comunidade para que as mulheres entendam a tecnologia e construam suas idéias.
  - [Facebook](https://www.facebook.com/railsgirlsbelem)
  - [Instagram](https://www.instagram.com/railsgirlsbelem)
  - [Site](http://railsgirls.com/belem)  

- **PyManas**: É uma comunidade para motivação, aprendizado, desafios e dicas sobre Python.  
  - [Instagram](https://www.instagram.com/pymanas)
 
- **IEEE WIE UFPA**: O Ramo Estudantil IEEE UFPA Women in Engineering tem como objetivo incentivar a participação feminina na área de engenharia. 
  - [Instagram](https://www.instagram.com/ieeewieufpa)
  - [Facebook](https://www.facebook.com/ieeewieufpa/)
  
- **Tech Manas**: Projeto Meninas na Computação da UFPA campus de Tucuruí/PA. Estimula a participação de mais mulheres nas áreas de computação e exatas.  :triangular_flag_on_post:
  - [Instagram](https://www.instagram.com/tech_manas)  

- **Programa Mulheres e Meninas nas Engenharias**: Grupo de mulheres acadêmicas de Engenharias da UFPA campus de Tucuruí/PA.  Estimula a participação de mais mulheres nas áreas de computação e exatas. :triangular_flag_on_post:
  - [Instagram](https://www.instagram.com/promulheremeninanaeng.camtuc/) 
  - [Facebook](https://www.facebook.com/Programa-Mulheres-e-Meninas-nas-Engenharias-105118171278560) 
  - [Linkedin](https://www.linkedin.com/company/pmme) 
  - [Youtube](https://youtube.com/channel/UCJPPyXMSU8Qlq7al4mIgBLg) 
 
# Segurança, Redes, Infraestrutura e Tecnologia em geral
- **Forense Pai d’Égua!**  
  - [WhatsApp](https://chat.whatsapp.com/DhI1yqLvxXRFF4ZF3wLHLy)

- **OWASP Belém**  
  - [Instagram](https://www.instagram.com/owasp_belem/)
  - [Facebook](https://www.facebook.com/OwaspBelem21)
  - [Site](https://owasp.org/www-chapter-belem/) 

- **Xibé Tech**: É Um ponto de encontro entre Profissionais do Mercado, Academia, Estudantes e Entusiastas da área de Tecnologia da Informação. O projeto foi criado pensando no desenvolvimento da nossa região e conta com a solidariedade de diversos profissionais na organização, tutoria ou simples participação e discussão dos mais variados tópicos. :triangular_flag_on_post: 
  - [Discord](https://discord.gg/s6j3vnGjmE)

# Gerenciamento de Projetos
- **CGP - Comunidade de Gerenciamento de Projetos Pará**: É um grupo sem fins lucrativos que pretende ampliar as oportunidades de qualificação técnica da região na área, principalmente pela socialização de informações relevantes. :triangular_flag_on_post:
  - [WhatsApp](https://chat.whatsapp.com/5lXO9dWLf9UAGgkLfroq5d)
  - [Instagram](https://www.instagram.com/cgppara/)
  - [Facebook](https://www.facebook.com/cgppara/)
  - [Youtube](https://www.youtube.com/channel/UCuUKvyUKHiD56ZWtettcvgA)
  - [Site](http://cgppara.blogspot.com/) 

# Capítulos locais de organizações internacionais
- **GDG Belém**: Somos o Google Developers Group oficial de Belém
  - [MeetUp](https://www.meetup.com/pt-BR/gdgbelemio)
  - [Facebook](https://www.facebook.com/GDGBelem)  
  - [Instagram](https://www.instagram.com/gdgbelemoficial)
  - [Twitter](https://twitter.com/GDGBelem)
  - [GitHub.io](https://gdgbelem.github.io/home/)
  - [Slack](https://gdgbelem.herokuapp.com)
  
- **Legal Hackers Belém**: Primeiro capítulo paraense do movimento global Legal Hackers! Conectando o Direito, Tecnologia e Inovação.
  - [Instagram](https://www.instagram.com/legalhackersbelem)
  - [WhatsApp](https://chat.whatsapp.com/LLyTAtzoQ9M3TyuwsZo1Rg)
  - [Facebook](https://www.facebook.com/belemlegalhackers)
  - [Twitter](https://twitter.com/legalhackersbel)        

- **PMI Branch Pará**: É a ramificação do PMI (Project Management Institute) Amazônia Chapter no Estado do Pará.
  - [Site](http://pmiam.org/para-branch/)
  - [Instagram](https://www.instagram.com/pmiamoficial)
  - [Facebook](https://www.facebook.com/bpmiam)
  - [Twitter](https://twitter.com/pmiam)

- **IEEE UFPA**: Ramo Estudantil da IEEE (Institute of Electrical and Electronics Engineers) na UFPA. :triangular_flag_on_post:
  - [Instagram](https://www.instagram.com/ieeeufpa)
  - [Facebook](https://www.facebook.com/ieeeufpa)

# Associações Empresariais, Empreendedorismo e Sindicato
- **Açaí Valley**: Associação Paraense de Tecnologia e Inovação
  - [Site](http://www.acaivalley.org)
  - [Facebook](https://www.facebook.com/acaivalley)
  - [Instagram](https://www.instagram.com/acaivalley)
  - [Telegram](https://t.me/acaivalley)
  - [Slack](https://bit.ly/slackcomunidadeacaivalley)

- **ParaTic**: Associação das Empresas Paraenses de Software e Tecnologia da Informação e Comunicação
  - [Site](http://www.paratic.com.br)
  - [Facebook](https://www.facebook.com/paraticbrasil)  
  - [Instagram](https://www.instagram.com/associacao_paratic)
    
- **Bate Papo Paidegua**: É uma comunidade que foi fundada para fortalecer o ecossistema empreendedor paraense. 
  - [Site](https://www.batepapopaidegua.com.br/)
  - [Instagram](https://www.instagram.com/batepapopaidegua/)
    
- **SINDPD-PA**: Sindicato dos Trabalhadores e Trabalhadoras em Tecnologias da Informação no Estado do Pará. 
  - [Site](https://www.sindpdpa.org.br/)
  - [Instagram](https://www.instagram.com/redesindpdpa)
  - [Facebook](https://www.facebook.com/redesindpdpa)
  - [Twitter](https://twitter.com/sindpdpa)
   
# UI/UX Design
- **Comunidade UI/UX Belém**
  - [Telegram](https://t.me/uiuxbelem)
